# -*- coding: utf-8 -*-
import csv
import os

db.define_table('training_information',
                Field('course_title', 'string'),
                Field('course_description', 'text'),
                Field('course_duration', 'string'),
                Field('admission_requirements', 'string'),
                Field('institution', 'string'),
                Field('campus', 'string'),
                Field('cost_of_course', 'string'),
                Field('file_location', 'string'),
                Field('user_id', 'reference auth_user'),
                Field('status', 'string', default='Approved'),
                Field('Approver', 'reference auth_user', default=auth.user and auth.user.id))

if db(db.training_information).isempty():
    db.training_information.import_from_csv_file(open('applications/WSIS/static/Boland_College/Boland_College.csv', 'r'), delimiter='|')
    db.training_information.import_from_csv_file(open('applications/WSIS/static/GetOn_Skills_Development_Centre/GetOn_Skills_Development_Centre.csv', 'r'), delimiter='|')
    db.training_information.import_from_csv_file(open('applications/WSIS/static/Northlink_College/Northlink_College.csv', 'r'), delimiter='|')

db.define_table('rates',
    Field('address', 'string', default=''),
    Field('usertime', 'datetime', default=request.now),
    Field('search_term','string', default=''),
    Field('interest','string', default=''),
    Field('user_id', 'reference auth_user', default=auth.user and auth.user.id),
    Field('item_id', 'integer'),
    Field('clicks', 'integer', default=0),
    migrate=True,)

db.define_table('transit',
    Field('item_id', 'integer', default=0),
    Field('search_term', 'string', default=''),
    migrate=True,)

db.define_table('training_submission',
                Field('course_title', 'string', requires=IS_NOT_EMPTY(error_message='please enter course title')),
                Field('course_description', 'text', requires=IS_NOT_EMPTY(error_message='please enter course description')),
                Field('course_duration', 'string', requires=IS_NOT_EMPTY(error_message='please enter course duration')),
                Field('admission_requirements', 'string', requires=IS_NOT_EMPTY(error_message='please enter course admission requirements')),
                Field('institution', 'string'),
                Field('campus', 'string', requires=IS_NOT_EMPTY(error_message='please enter campus where course is administered')),
                Field('cost_of_course', 'string', requires=IS_NOT_EMPTY(error_message='please enter cost of course')),
                Field('file_location', 'upload', autodelete=True, uploadseparate=True, uploadfolder=os.path.join(request.folder,'uploads')),
                Field('user_id', 'reference auth_user', default=auth.user and auth.user.id),
                Field('status', 'string', default='Pending Approval'))

db.training_submission.user_id.writable = db.training_submission.user_id.readable = False
db.training_submission.status.writable=False
