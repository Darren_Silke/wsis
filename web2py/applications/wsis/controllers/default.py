# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

# -------------------------------------------------------------------------
# This is the default.py controller
# - index is the default action of any application
# - user is required for authentication and authorization
# - download is for downloading files uploaded in the db (does streaming)
# -------------------------------------------------------------------------

from gluon.contrib.user_agent_parser import mobilize
import solr
import os

@mobilize
def index():
    # If a user is logged in and the user has 'institution' or 'admin' role, redirect them to the corresponding page.
    if auth.has_membership(role='admin'):
        redirect(URL('superadmin'))
    elif auth.has_membership(role='institution'):
        redirect(URL('institutions'))
    #else it is a normal user

    form = FORM(INPUT(_type = 'string', _placeholder = 'Enter Search Term', _name = 'term', requires = IS_NOT_EMPTY(error_message='Enter Search Term')))
    search_term = None
    display_text = False
    display_button = False
    rows = []
    page = 0
    items_per_page = 0
    rows_to_display = []

    if len(request.args) != 0:
        if ':' in request.vars.search_term:
            request.vars.search_term = request.vars.search_term.replace(':', '\:')
        solr_connection = solr.SolrConnection('http://localhost:8983/solr/traininginformation')
        search_term = request.vars.search_term
        solr_response = solr_connection.query(search_term, rows = 2147483647)
        if '\:' in search_term:
            search_term = search_term.replace('\:', ':')
        form.vars.term = search_term
        if len(solr_response.results) == 0:
            display_text = True
        else:
            for result in solr_response.results:
                row = db(db.training_information.id == result['id']).select().first()
                rows.append(row)
            page = int(request.args[0])
            items_per_page = 10
            start = (page - 1) * items_per_page
            end = (page) * items_per_page + 1
            for i in range(start, end):
                if i == len(rows):
                    break
                rows_to_display.append(rows[i])
            # Display the 'What users with similar interests searched for' menu item if the search returns some results.
            response.menu.extend([(T('What users with similar interests searched for'), True, URL('default', 'recommended'), [])])
            # Pass the search term to a database to be stored temporarily.
            # The search term will be used by the recommender and other functions.
            db.transit.update_or_insert((db.transit.id == 1), search_term = search_term)
            display_button = True

    if form.process().accepted:
        search_term = form.vars.term
        redirect(URL('index/1', vars = dict(search_term = search_term)))
    elif form.errors:
        response.flash = None

    return dict(search_term = search_term, display_text = display_text, display_button = display_button, form = form, page = page, items_per_page = items_per_page, rows_to_display = rows_to_display)


def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/bulk_register
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    also notice there is http://..../[app]/appadmin/manage/auth to allow administrator to manage users
    """
    return dict(form=auth())


@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()


@auth.requires_membership('institution')
def institutions():
    db.training_submission.user_id.writable = False
    user_uploads = SQLFORM.grid(db.training_submission.user_id == auth.user.id,
                       args=[auth.user.id],
                       searchable=True,
                       deletable=True,
                       details=True,
                       selectable=False,
                       csv=False,
                       user_signature=False, formstyle='divs')

    return dict(user_uploads=user_uploads)


@auth.requires_membership('admin')
def list_uploads():
    response.menu.extend([(T('Central Admin'), True, URL('default', 'superadmin'), [])])
    db.training_submission.user_id.readable = True
    #db.training_submission.status.writable = True
    btn = lambda row: BUTTON_TO("Approve", URL('approve_submission',
                                               args=row.training_submission.id,
                                               vars=dict(inst_member=row.training_submission.user_id)))
    db.training_submission.approve = Field.Virtual(btn)
    form = SQLFORM.grid(db.training_submission.user_id,
                       args=[],
                       create=False,
                       searchable=True,
                       deletable=False,
                       details=True,
                       selectable=False,
                       csv=False,
                       user_signature=True, formstyle='divs')
    response.flash=None
    return dict(form=form)


def rate():
    #If user is not logged in, do not track their activity
    if not auth.user:
        row =  db(db.training_information.id == request.args(0)).select().first()
        redirect(URL(r=request, c='static', f=row.file_location))
    else:
        myrow=""
        #Keep record of the result that the user clicks on in their session
        try:
            prev_ses_id = db((db.rates.user_id==auth.user.id) & (db.rates.item_id==request.args(0))).select(db.rates.address, db.rates.usertime)
            #Calculate the time it took the same user to search click on the same item, and whether they used the same IP address
            if prev_ses_id:
                dif = request.now - prev_ses_id[0].usertime
                minutes, seconds = divmod(dif.seconds, 60)
                weeks, days = divmod(dif.days, 7)
                hours, minutes = divmod(minutes, 60)
                print str(hours)
                print str(prev_ses_id)
                #Record the item clicked on by the user and their IP address
                itemid = db.rates.update_or_insert((db.rates.user_id==auth.user.id) &
                           (db.rates.item_id==request.args(0)), item_id=request.args(0), address=request.env.remote_addr, usertime=request.now)
                #
                myrow = db((db.rates.user_id==auth.user.id) & (db.rates.item_id==request.args(0)) &
                           (hours>0) & ~(db.rates.address==prev_ses_id[0].address)).update(search_term=db.rates.search_term + ' ' +
                                                                                           str(request.vars.search_term),
                                                                                           clicks=db.rates.clicks+1)
            else:
                itemid = db.rates.update_or_insert((db.rates.user_id==auth.user.id) &
                                                   (db.rates.item_id==request.args(0)),
                                                   item_id=request.args(0),
                                                   address=request.env.remote_addr,
                                                   usertime=request.now)

                myrow = db((db.rates.user_id==auth.user.id) &
                           (db.rates.item_id==request.args(0))).update(search_term=db.rates.search_term + ' ' +
                                                                       str(request.vars.search_term),interest=db.rates.interest + ' '+
                                                                       str(request.vars.search_term), clicks=db.rates.clicks+1)
        except ValueError:
            return str(ValueError)

        #Store the clicked-on course and the search term in a temporary database to be accessed by the recommender function
        t = db.transit.update_or_insert((db.transit.id==1),item_id=request.args(0), search_term=request.vars.search_term)
        # Redirect to page after above lines of code are executed.
        row =  db(db.training_information.id == request.args(0)).select().first()
        redirect(URL(r=request, c='static', f=row.file_location))


def recommended():
    response.menu.extend([(T('What users with similar interests searched for'), True, URL('default', 'recommended'), [])])
    getcourseids=""
    c = ""
    q =""
    i=0
    s=[]
    ct=""
    #If user is logged in, recommend to them courses clicked on by others excluding what they clicked on,
    #Else, there's nothing to recommend.
    if not auth.user:
        row = db(db.transit.id==1).select(db.transit.search_term)
        ct = row[0].search_term
        query = db.rates.search_term.contains(row[0].search_term)
        results = db(query).select(db.rates.user_id, orderby=~db.rates.clicks)
        if len(results)>0:#If there are any other users that searched for the item?
            maxrating = results[0].user_id #Get user with top number of clicks
        else:
            maxrating = 0
        if maxrating > 0:
            constraints = (db.rates.user_id==maxrating) & ~(db.rates.search_term.contains(row[0].search_term))
            getcourseids = db(constraints).select(db.rates.item_id, orderby=db.rates.item_id)

            #Use course IDs to retrieve the course details
            s = db(db.training_information.id.belongs([c.item_id for c in getcourseids])).select(db.training_information.id,
                                                                                                 db.training_information.course_title,
                                                                                                 db.training_information.cost_of_course,
                                                                                                 db.training_information.admission_requirements,
                                                                                                 db.training_information.campus,
                                                                                                 db.training_information.file_location)
        return dict(s=s, ct=ct)
    else:
            #Use the user's selected result to recommend what others searched for,
        #filter similar searches using the searched term
        try:
            row = db(db.transit.id == 1).select(db.transit.item_id, db.transit.search_term)
            ct = row[0].search_term
            query = (db.rates.item_id==row[0].item_id) & ~(db.rates.user_id==auth.user.id)
            results = db(query).select(db.rates.user_id, orderby=~db.rates.clicks)
            if len(results)>0:#If there are any other users that searched for the item?
                maxrating = results[0].user_id #Get user with top number of clicks
            else:
                maxrating = 0
            if maxrating > 0:
                constraints = (db.rates.user_id==maxrating) & ~(db.rates.item_id==row[0].item_id)
                getcourseids = db(constraints).select(db.rates.item_id, orderby=db.rates.item_id)

                #Use course IDs to retrieve the course details
                s = db(db.training_information.id.belongs([c.item_id for c in getcourseids])).select(db.training_information.id,
                                                                                                     db.training_information.course_title,
                                                                                                     db.training_information.cost_of_course,
                                                                                                     db.training_information.admission_requirements,
                                                                                                     db.training_information.campus,
                                                                                                     db.training_information.file_location)
        except ValueError:
            return str(ValueError)
        return dict(s=s, ct=ct)


def final():
    #If the user clicks on the recommended course, redirect them to the site.
    redirect(URL(r=request, c='static', f=request.vars.loc))
    return dict()


def sendmail():
    response.menu.extend([(T('What users with similar interests searched for'), True, URL('default', 'recommended'), [])])
    course_name = request.vars.course
    form = SQLFORM.factory(Field('email', label='Enter your Email Address',
                                 requires=[IS_EMAIL(error_message='Enter a valid Email Address'),
                                           IS_NOT_EMPTY(error_message='Enter Your Email Address')]),
                           submit_button='Send', formstyle='divs')
    if form.process().accepted:
        from gluon.tools import Mail
        mail = Mail()
        mail.settings.server = 'smtp.cs.uct.ac.za:25' or 'logging'
        mail.settings.sender = 'slkdar001@cs.uct.ac.za'
        mail.settings.login = 'slkdar001:DrS12037897!'
        row = db(db.training_information.id==request.args(0)).select()
        context=dict(row=row)
        mail.send(to=[form.vars.email], subject='Self-Recommended Course from WSIS',
                  message=['Dear User',
                           '<html><head>' +
                           '<style>table, th, td {border: 1px solid DarkOrange; border-collapse: collapse;}</style>' +
                           '</head><body><h2 style="color:green">' +
                           'You sent this course to yourself from WSIS</h2>' +
                           '<table border="1" width="100%"><caption><strong>Course Summary</strong></caption><tr><th><h3><a href=' +
                           URL(r=request, c='static', f=str(row[0].file_location)) +' >'
                           + str(row[0].course_title) +
                           '</th><th style="background-color:AntiqueWhite"><strong>Cost Of Course: </strong>' +
                           row[0].cost_of_course + '</th></tr><tr><td><strong>Duration</strong></td><td>' +
                           row[0].course_duration + '</td></tr><tr><td><strong>Institution</strong></td><td>' +
                           row[0].institution + '</td></tr><tr><td><strong>Admission Requirements</strong></td><td>' +
                           row[0].admission_requirements + '</td></tr><tr><td><strong>Campus</strong></td><td>' +
                           row[0].campus + '</td></tr><tr><td><strong>Course Description</strong></td><td>' +
                           row[0].course_description + '</td></tr></table></h3></body></html>'])
        if mail:
            session.flash='Email successfully sent to the email address you provided'
            redirect(URL('recommended', args=request.args))
        else:
            session.flash='Could not send email, please try again or contact your system administrator.'
    return dict(form=form, course_name=course_name)


def sendmailauth():
    mail = auth.settings.mailer
    mail.settings.server = 'smtp.cs.uct.ac.za:25' or 'logging'
    mail.settings.sender = 'slkdar001@cs.uct.ac.za'
    mail.settings.login = 'slkdar001:DrS12037897!'
    row = db(db.training_information.id==request.args(0)).select()
    mail.send(to=[auth.user.email], subject='Self-Recommended Course from WSIS',
              message=['Dear User',
                           '<html><head>' +
                           '<style>table, th, td {border: 1px solid DarkOrange; border-collapse: collapse;}</style>' +
                           '</head><body><h2 style="color:green">' +
                           'You sent this course to yourself from WSIS</h2>' +
                           '<table border="1" width="100%"><caption><strong>Course Summary</strong></caption><tr><th><h3><a href=' +
                           URL(r=request, c='static', f=str(row[0].file_location)) +' >'
                           + str(row[0].course_title) +
                           '</th><th style="background-color:AntiqueWhite"><strong>Cost Of Course: </strong>' +
                           row[0].cost_of_course + '</th></tr><tr><td><strong>Duration</strong></td><td>' +
                           row[0].course_duration + '</td></tr><tr><td><strong>Institution</strong></td><td>' +
                           row[0].institution + '</td></tr><tr><td><strong>Admission Requirements</strong></td><td>' +
                           row[0].admission_requirements + '</td></tr><tr><td><strong>Campus</strong></td><td>' +
                           row[0].campus + '</td></tr><tr><td><strong>Course Description</strong></td><td>' +
                           row[0].course_description + '</td></tr></table></h3></body></html>'])
    if mail:
        session.flash='Email was successfully sent to your registered email address'
        redirect(URL('recommended', args=request.args))
    else:
        session.flash='Could not send email, please try again or contact your system administrator.'


@auth.requires_membership('admin')
def list_users():
    response.menu.extend([(T('Central Admin'), True, URL('default', 'superadmin'), [])])
    btn = lambda row: A("Edit", _href=URL('manage_user', args=row.auth_user.id))
    db.auth_user.edit = Field.Virtual(btn)
    rows = db(db.auth_user).select()
    headers = ["ID", "Name", "Last Name", "Email", "Edit"]
    fields = ['id', 'first_name', 'last_name', "email", "edit"]
    table = TABLE(THEAD(TR(*[B(header) for header in headers])),
                  TBODY(*[TR(*[TD(row[field]) for field in fields]) \
                        for row in rows]))
    table["_class"] = "table table-striped table-bordered table-condensed"
    return dict(table=table)


@auth.requires_membership('admin')
def manage_user():
    response.menu.extend([(T('Central Admin'), True, URL('default', 'superadmin'), [])])
    user_id = request.args(0) or redirect(URL('list_users'))
    form = SQLFORM(db.auth_user, user_id, formstyle='divs').process()
    membership_panel = LOAD(request.controller,
                            'manage_membership.html',
                             args=[user_id],
                             ajax=True)
    return dict(form=form,membership_panel=membership_panel)


@auth.requires_membership('admin')
def manage_membership():
    user_id = request.args(0) or redirect(URL('list_users'))
    db.auth_membership.user_id.default = int(user_id)
    db.auth_membership.user_id.writable = False
    form = SQLFORM.grid(db.auth_membership.user_id == user_id,
                       args=[user_id],
                       searchable=False,
                       deletable=True,
                       details=False,
                       selectable=False,
                       csv=False,
                       user_signature=False)  # change to True in production
    return form

@auth.requires_membership('admin')
def superadmin():
    j=""
    r = db(db.auth_user.institution_access==True).select(db.auth_user.id)
    s = db((db.auth_membership.user_id.belongs([i.id for i in r])) & ~(db.auth_membership.group_id==5)).select(db.auth_membership.user_id)
    j = db(db.auth_user.id.belongs([c.user_id for c in s])).select(db.auth_user.id, db.auth_user.first_name, db.auth_user.institution_name)
    response.menu = []
    return dict(j=j, r=r, s=s)

@auth.requires_membership('admin')
def approve_submission():
    #Approves the course submitted by an institution
    db.training_submission.status.writable = True
    course = db(db.training_submission.id==request.args(0)).select()
    exists = db((db.training_information.course_title==course[0].course_title) & (db.training_information.user_id==course[0].user_id)).count()
    if exists==0:#If the course was not approved before?
        #Move course from the temporary storage to the indexable data store
        movecourse = db.training_information.insert(course_title=course[0].course_title,
                                                    course_description=course[0].course_description,
                                                    course_duration=course[0].course_duration,
                                                    admission_requirements=course[0].admission_requirements,
                                                    institution=course[0].institution, campus=course[0].campus,
                                                    cost_of_course=course[0].cost_of_course,
                                                    file_location=course[0].file_location, user_id=course[0].user_id)
        if movecourse:#if the course was successfully moved?
            #Update approval status
            approve = db(db.training_submission.id==request.args(0)).update(status='Approved')
            if approve:
                redirect(URL('list_uploads', args=request.args))
    else:
        redirect(URL('list_uploads', args=request.args))
