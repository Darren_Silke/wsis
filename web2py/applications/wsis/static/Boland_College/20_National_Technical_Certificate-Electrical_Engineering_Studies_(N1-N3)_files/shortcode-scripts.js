        jQuery(document).ready(function($) {
            $('#bcer-form input[type="submit"].button').click(function(e){
                e.preventDefault();
                $('#bcer-form input[type="submit"].button').addClass('av-sending-button');
                $('#bcer-form input[type="submit"].button').attr('disabled', 'disabled');
                
                if( !$.trim($('#Student_Number').val()) || !$.trim($('#ID_Number').val()) ){ //validate the form fields
                    $('#bcer-form .form_element .text_input').each(function(){
                        if( !$.trim($(this).val()) ) {
                            $(this).parent().addClass('error');
                        }
                        
                        $(this).focus(function(){
                            $(this).parent().removeClass('error');
                        });
                    });
                    
                    $('#bcer-form input[type="submit"].button').removeClass('av-sending-button');
                    $('#bcer-form input[type="submit"].button').removeAttr('disabled');
                    
                    alert('Please fill in both the required form fields before submitting.');
                }else{
                    $.ajax({
                        type: "POST",
                        url: plugin_data.fetch_results_script_url,
                        data: { 
                            wp_root: plugin_data.wp_root,
                            s_num: $.trim($('#Student_Number').val()),
                            id_num: $.trim($('#ID_Number').val())
                        },
                        success: function(result){
                            $('#bcer-form input[type="submit"].button').removeClass('av-sending-button');
                            $('#bcer-form input[type="submit"].button').removeAttr('disabled');
                    
                            var res = $.trim(result);
                            
                            if(res !== "error") {
                                $('#exam-results').html(res);
                                $('.bcer_ajax_form fieldset').slideUp();
                            }else{
                                alert('No matched results were found. Please make sure all the required form fields are filled in correctly.');
                            }
                        }
                    });
                }
                
                
            });
            
            
        }); /* dont get rid of this*/