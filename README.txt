WHAT SHOULD I STUDY?

DESCRIPTION:

'What Should I Study?' is a domain-specific information retrieval system that provides information about training opportunities in South Africa. The information is harvested, indexed, stored and ultimately used to provide relevant results to users when they search for training information through an easy-to-use interface which can be accessed using a desktop computer or mobile phone. The intention of 'What Should I Study?' is to bridge the communication gap between low-skilled unemployed people, and people or organizations that provide training.

SOFTWARE PREREQUISITES:

1. Java 8
2. Python 2.7.12
3. solrpy 0.9.7

INSTRUCTIONS:

The instructions below apply to Linux operating systems.

1. Launch Apache Solr
	a. Open a terminal instance in the "Apache Solr" folder.
	b. Type the following: bin/solr start
2. Launch web2py
	a. Open a terminal instance in the "web2py" folder.
	b. Type the following: python web2py.py
	c. Follow the resulting instructions that appear in the terminal instance.
Further information about specifying parameters before launching web2py can be found by typing the following in the terminal instance: python web2py.py -h

To stop Apache Solr, type the following in the terminal instance that was used to launch Apache Solr: bin/solr stop -all
To stop web2py, follow the instructions in the terminal instance that was used to launch web2py.

