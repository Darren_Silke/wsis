import scrapy
import pdfquery
from tutorial.items import DmozItem
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import HtmlXPathSelector
from scrapy.item import Item
from scrapy.spider import BaseSpider
from scrapy import log


class DmozSpider(scrapy.Spider):
    name = "dmoz"
    allowed_domains = ["northlink.org.za"]
    start_urls = ["http://www.northlink.co.za/Courses.aspx"]
    rules = [Rule(SgmlLinkExtractor(allow=()), 
                  follow=True),
             Rule(SgmlLinkExtractor(allow=()), callback='parse_item')
    ]

    def parse(self, response):
        hxs = HtmlXPathSelector(response)
        #filename = response.url.split("/")[4] + '.html'
        with open('example1.html', 'wb') as f:
            f.write(response.body)
        #items = []
        #for title in titles:
            #item = response.body
            #item["title"] = title.select("a/text()").extract()
            #item["link"] = title.xpath("a/@href").extract()
            #items.append(item)
        #return(response.body)
          
