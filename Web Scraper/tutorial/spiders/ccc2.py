import scrapy
from tutorial.items import DmozItem

class DmozSpider(scrapy.Spider):
    name = "dmoz"
    allowed_domains = ["northlink.co.za"]
    start_urls = ["http://www.northlink.co.za/Courses.aspx"]

    def parse(self, response):
        for href in response.css("ul.directory.dir-col > li > a::attr('href')"):
            url = response.urljoin(href.extract())
            yield scrapy.Request(url, callback=self.parse_dir_contents)


    def parse_dir_contents(self, response):
        for sel in response.xpath('//td'):
            item = DmozItem()
            item['title'] = sel.xpath('a/text()').extract()
            item['link'] = sel.xpath('a/@href').extract()
            item['desc'] = sel.xpath('text()').extract()
            #with open('Courses_NorthLink_College.txt', 'a+') as c:
                #c.write(str(title) + '\n')
            yield item

            def parse_articles_follow_next_page(self, response):
                for article in response.xpath("//article"):
                    item = ArticleItem()
                    item['title'] = sel.xpath('a/text()').extract()
                    item['link'] = sel.xpath('a/@href').extract()
                    item['desc'] = sel.xpath('text()').extract()
                    #with open('Courses_NorthLink_College.txt', 'a+') as c:
                        #c.write(str(title) + '\n')
                next_page = response.css("ul.navigation > li.next-page > a::attr('href')")
                if next_page:
                    url = response.urljoin(next_page[0].extract())
                    yield scrapy.Request(url, self.parse_articles_follow_next_page)
