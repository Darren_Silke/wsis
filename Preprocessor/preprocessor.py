from HTMLParser import HTMLParser
from templater import Templater
import glob
import sys


class MLStripper(HTMLParser):
    def __init__(self):
        self.reset()
        self.fed = []

    def handle_data(self, d):
        self.fed.append(d)

    def get_data(self):
        return ''.join(self.fed)


def strip_tags(html):
    s = MLStripper()
    s.feed(html)
    return s.get_data()


def northlink_college():
    # Create templates
    course_title_template = Templater(
        template=[None, '<span id="MainContent_FormView1_titleLabel">', None, '</span>'])
    course_description_template = Templater(
        template=[None, '<span id="MainContent_FormView1_LabelTextIntro"><p>', None, '</span>'])
    course_duration_template = Templater(
        template=[None, '<span id="MainContent_FormView1_RepeaterProgrammeAvailability_DurationLabel_0">', None,
                  '</span>'])
    multiple_course_durations_template = Templater(
        template=[None, '<span id="MainContent_FormView1_RepeaterProgrammeAvailability_DurationLabel_1">', None,
                  '</span>'])
    admission_requirements_template = Templater(
        template=[None, '<span id="MainContent_FormView1_LabelAdmissionRequirements"><p>', None, '</span>'])
    campus_template = Templater(
        template=[None, '<span id="MainContent_FormView1_RepeaterProgrammeAvailability_AvailCampusLabel_0">', None,
                  '</span>'])

    for file_name in files:
        # Set default variables
        course_title = 'Not specified'
        course_description = 'Not specified'
        course_duration = 'Not specified'
        multiple_course_durations = False
        admission_requirements = 'Not specified'
        institution = 'Northlink College'
        campus = 'Not specified'
        cost_of_course = 'Not specified'
        file_location = 'Northlink_College/' + file_name[file_name.rindex('\\') + 1:-5] + '.html'

        # Open file for reading
        in_file = open(file_name, 'r')

        # Process each line in file
        for line in in_file.readlines():
            try:
                course_title = course_title_template.parse(line)[1]
            except ValueError:
                pass
            try:
                course_description = course_description_template.parse(line)[1]
            except ValueError:
                pass
            try:
                course_duration = course_duration_template.parse(line)[1]
            except ValueError:
                pass
            try:
                multiple_course_durations_template.parse(line)[1]
                multiple_course_durations = True
            except ValueError:
                pass
            try:
                admission_requirements = admission_requirements_template.parse(line)[1]
            except ValueError:
                pass
            try:
                campus = campus_template.parse(line)[1]
            except ValueError:
                pass

        # Close file
        in_file.close()

        # Post-processing of data
        if course_title != 'Not specified':
            course_title = course_title.replace('&amp;', '&')
        if course_description != 'Not specified':
            course_description = course_description.replace('&nbsp;', ' ')
            course_description = strip_tags(course_description)
        if multiple_course_durations:
            course_duration = 'Varies depending on programme'
        if admission_requirements != 'Not specified':
            admission_requirements = strip_tags(admission_requirements)

        out_file.write(course_title + '|' + course_description + '|' + course_duration + '|' + admission_requirements +
                       '|' + institution + '|' + campus + '|' + cost_of_course + '|' + file_location + '\n')


def boland_college():
    # Create templates
    course_title_template = Templater(
        template=[None, '</div><h1 class="av-special-heading-tag" itemprop="headline">',
                  None, '</h1><div class="special-heading-border">'
                        '<div class="special-heading-inner-border"></div></div></div>'])
    course_description_template = Templater(template=[None, '<section class="av_textblock_section" '
                                                            'itemscope="itemscope" '
                                                            'itemtype="https://schema.org/CreativeWork">'
                                                            '<div class="avia_textblock " itemprop="text"><p>',
                                                      None,
                                                      '</p>'])
    course_duration_template = Templater(template=[None, 'Duration</h4></header><div class="iconlist_content " '
                                                         'itemprop="text"><p>', None, '</p>'])
    admission_requirements_template = Templater(template=[None, 'Requirements</h4></header>'
                                                                '<div class="iconlist_content " itemprop="text"><p>',
                                                          None, '</p>'])

    for file_name in files:
        # Set default variables
        course_title = 'Not specified'
        course_description = 'Not specified'
        course_duration = 'Not specified'
        admission_requirements = 'Not specified'
        institution = 'Boland College'
        campus = 'Not specified'
        cost_of_course = 'Not specified'
        file_location = 'Boland_College/' + file_name[file_name.rindex('\\') + 1:-5] + '.html'

        # Open file for reading
        with open(file_name, 'r') as in_file:
            # Process each line in file
            lines = in_file.readlines()
            for i in range(0, len(lines)):
                line = lines[i]
                try:
                    course_title = course_title_template.parse(line)[1]
                except ValueError:
                    pass
                try:
                    course_description = course_description_template.parse(line)[1]
                except ValueError:
                    pass
                try:
                    course_duration = course_duration_template.parse(line)[1]
                except ValueError:
                    if 'Duration</h4></header><div class="iconlist_content " itemprop="text"><ul>' in line:
                        course_duration = ''
                        exit_loop = False
                        j = 1
                        while not exit_loop:
                            if lines[i + j] == '</ul>\n':
                                exit_loop = True
                            else:
                                course_duration += lines[i + j]
                                j += 1
                        course_duration = course_duration.replace('\n', '. ').strip()
                    pass
                try:
                    admission_requirements = admission_requirements_template.parse(line)[1]
                except ValueError:
                    pass

        in_file.close()

        # Post-processing of data
        if course_title != 'Not specified':
            course_title = course_title.replace('<span class="special_amp">&amp;</span>', '&')
        if course_duration != 'Not specified':
            course_duration = strip_tags(course_duration)

        out_file.write(course_title + '|' + course_description + '|' + course_duration + '|' + admission_requirements +
                       '|' + institution + '|' + campus + '|' + cost_of_course + '|' + file_location + '\n')


def geton_skills_development_centre():
    # Create templates
    course_title_template = Templater(template=[None, '<title>', None, '</title>'])
    course_duration_template = Templater(template=[None, '>Duration: ', None, '</'])
    cost_of_course_template = Templater(template=[None, '>Cost: ', None, '</'])

    for file_name in files:
        # Set default variables
        course_title = 'Not specified'
        course_description = 'Not specified'
        course_duration = 'Not specified'
        admission_requirements = 'Not specified'
        institution = 'GetOn Skills Development Centre'
        campus = 'Pretoria'
        cost_of_course = 'Not specified'
        file_location = 'GetOn_Skills_Development_Centre/' + file_name[file_name.rindex('\\') + 1:-5] + '.html'

        # Open file for reading
        with open(file_name, 'r') as in_file:
            # Process each line in file
            lines = in_file.readlines()
            for i in range(0, len(lines)):
                line = lines[i]
                try:
                    course_title = course_title_template.parse(line)[1]
                except ValueError:
                    pass
                if course_title != 'Entrepreneurial Course' and i == 444:
                    course_description = line
                elif course_title == 'Entrepreneurial Course' and i == 445:
                    course_description = line
                try:
                    course_duration = course_duration_template.parse(line)[1]
                except ValueError:
                    pass
                try:
                    cost_of_course = cost_of_course_template.parse(line)[1]
                except ValueError:
                    pass

        in_file.close()

        # Post-processing of data
        if course_description != 'Not specified':
            course_description = strip_tags(course_description).replace('\n', '')

        print 'Course Title: ' + course_title
        print 'Course Description: ' + course_description
        print 'Course Duration: ' + course_duration
        print 'Admission Requirements: ' + admission_requirements
        print 'Institution: ' + institution
        print 'Campus: ' + campus
        print 'Cost Of Course: ' + cost_of_course
        print 'File Location: ' + file_location + '\n'

        out_file.write(course_title + '|' + course_description + '|' + course_duration + '|' + admission_requirements +
                       '|' + institution + '|' + campus + '|' + cost_of_course + '|' + file_location + '\n')


# Get path to files from user and process files
path_to_files = raw_input('Enter path to directory: ')
files = sorted(glob.glob(path_to_files + '/*.html'))
if len(files) == 0:
    print 'Invalid path to directory.\nAborting...'
    sys.exit()

institution_name = raw_input('Enter institution name: ')
if institution_name.lower() == 'northlink college':
    out_file = open(path_to_files + '/Northlink_College.csv', 'w')
    out_file.write('course_title|course_description|course_duration|admission_requirements|institution|campus|'
                   'cost_of_course|file_location\n')
    northlink_college()
    out_file.close()
elif institution_name.lower() == 'boland college':
    out_file = open(path_to_files + '/Boland_College.csv', 'w')
    out_file.write('course_title|course_description|course_duration|admission_requirements|institution|campus|'
                   'cost_of_course|file_location\n')
    boland_college()
    out_file.close()
elif institution_name.lower() == 'geton skills development centre':
    out_file = open(path_to_files + '/GetOn_Skills_Development_Centre.csv', 'w')
    out_file.write('course_title|course_description|course_duration|admission_requirements|institution|campus|'
                   'cost_of_course|file_location\n')
    geton_skills_development_centre()
    out_file.close()
else:
    print 'Institution not supported.\nAborting...'
